use crate::occurrence::*;
use crate::parser::*;
#[derive(Default)]
pub struct StateMachine {
    version: Version,
    id: Id,
    states: States,
}
#[derive(Default)]
pub struct Version {}
pub struct Id(String);
pub struct Description(String);
#[derive(Default)]
pub struct States {
    id: Id,
    event: Event,
}
#[derive(Default)]
pub struct Event {}
impl StateMachine {
    pub fn construct(pb: &ParsingData) -> StateMachine {
        let mut state_machine = StateMachine::default();
        read_literal_string("StateMachine")(&pb).unwrap();
        state_machine.version = Version::construct(&pb);
        state_machine.id = Id::construct(&pb);
        read_literal_string("{")(&pb).unwrap();
        state_machine.states = States::construct(&pb);
        read_literal_string("}")(&pb).unwrap();
        state_machine
    }
}
impl Version {
    pub fn construct(pb: &ParsingData) -> Version {
        let mut version = Version::default();
        version
    }
}
impl Id {
    pub fn construct(pb: &ParsingData) -> Id {
        Self(
            read_arbitrary_letters(
                    String::from("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_"),
                    Occurrence::AtLeast(2u32),
                )(&pb)
                .unwrap(),
        )
    }
}
impl Description {
    pub fn construct(pb: &ParsingData) -> Description {
        Self(
            read_arbitrary_letters(
                    String::from("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_"),
                    Occurrence::Range(0u32, 4294967295u32),
                )(&pb)
                .unwrap(),
        )
    }
}
impl States {
    pub fn construct(pb: &ParsingData) -> States {
        let mut states = States::default();
        read_literal_string("State:")(&pb).unwrap();
        states.id = Id::construct(&pb);
        read_literal_string("events:")(&pb).unwrap();
        states.event = Event::construct(&pb);
        states
    }
}
impl Event {
    pub fn construct(pb: &ParsingData) -> Event {
        let mut event = Event::default();
        event
    }
}

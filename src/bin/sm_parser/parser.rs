use std::fmt::{Debug, Display, Formatter};
use std::ops::Range;
use std::str::FromStr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;
use crate::occurrence::Occurrence;


// TODO: ParsingData Iterator?
pub struct ParsingData
{
    data: String,
    next_element: AtomicUsize,
}

impl Display for ParsingData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let next_element = self.next_element.load(SeqCst);
        write!(f, "next element: {}", next_element).unwrap();
        write!(f, " remaining string: {}", &self.data[next_element..])
    }
}

impl Debug for ParsingData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self, f)
    }
}


impl ParsingData {
    pub fn new(data: String) -> ParsingData {
        Self {
            data,
            next_element: AtomicUsize::default(),
        }
    }

    fn get_next_range(&self, length: usize) -> Range<usize> {
        let range = Range { start: self.next_element.load(SeqCst), end: self.next_element.load(SeqCst) + length };
        self.advance(length);
        range
    }

    fn get_ranged(&self, length: usize) -> &str {
        let r = self.get_next_range(length);
        &self.data[r]
    }

    fn advance(&self, length: usize) {
        self.next_element.fetch_add(length, SeqCst);
    }

    fn current_position(&self) -> usize {
        self.next_element.load(SeqCst)
    }

    fn current_data(&self) -> &str { &self.data[self.current_position()..] }

    fn create_error(&self) -> Result<String, ParsingError> {
        Err(ParsingError::new(self.current_position(), self.current_data().get(..5).unwrap().to_string()))
    }

    fn create_error_with_message(&self, message: String) -> Result<String, ParsingError> {
        Err(ParsingError::new_with_message(self.current_position(), self.current_data().get(..5).unwrap().to_string(), message))
    }
}


#[derive(Debug)]
pub struct ParsingError {
    start_position: usize,
    message: String,
    first_bytes: String,
}

impl ParsingError {
    pub fn new(start_position: usize, first_bytes: String) -> ParsingError { Self::new_with_message(start_position, first_bytes, "".to_string()) }
    pub fn new_with_message(start_position: usize, first_bytes: String, message: String) -> ParsingError { Self { start_position, message, first_bytes } }
}

pub fn read_integer<T>(occurrence: Occurrence) -> impl Fn(&ParsingData) -> Result<T, <T as FromStr>::Err>
    where
        T: FromStr, <T as FromStr>::Err: Debug
{
    move |parsing_data| {
        let slice = read_arbitrary_letters("0123456789".to_string(), occurrence)(parsing_data).unwrap();
        // let slice = parsing_data.get_ranged(length as usize);
        slice.parse()
    }
}

pub fn read_literal_string(prefix_pattern: &str) -> impl Fn(&ParsingData) -> Result<String, ParsingError> + '_
{
    move |parsing_data| {
        match parsing_data.current_data().starts_with(&prefix_pattern) {
            true => {
                parsing_data.advance(prefix_pattern.len());
                Ok(prefix_pattern.to_string())
            }
            false => Err(ParsingError::new(parsing_data.current_position(), parsing_data.data.get(..5).unwrap().to_string()))
        }
    }
}

// join with read_between
pub fn read_arbitrary_letters(allowed_letters: String, occurrence: Occurrence) -> impl Fn(&ParsingData) -> Result<String, ParsingError> {
    move |parsing_data| {
        let mut result = String::default();
        for c in parsing_data.current_data().chars() {
            if allowed_letters.contains(c) {
                result.push(c);
                if occurrence.is_upper_boundary(result.len() + 1) { break; }
                continue;
            }
            break;
        }
        if occurrence.contains(result.len()) {
            parsing_data.advance(result.len());
            return Ok(result);
        }
        parsing_data.create_error()
    }
}

pub fn read_between(prefix: String, postfix: String) -> impl Fn(&ParsingData) -> Result<String, ParsingError>
{
    move |parsing_data| {
        if parsing_data.current_data().starts_with(&prefix) {
            let iter = &parsing_data.current_data().chars().skip(prefix.len()).into_iter().collect::<String>();
            match iter.find(&postfix) {
                Some(position) => {
                    let result = iter[prefix.chars().count() - 1..position].to_string();
                    parsing_data.advance(prefix.len() + result.len() + postfix.len());
                    return Ok(result);
                }
                None => {}
            }
        }
        parsing_data.create_error()
    }
}

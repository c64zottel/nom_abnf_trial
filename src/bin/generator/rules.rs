use std::fmt::{Debug, Formatter};
use std::slice::Iter;

use nom::error::ErrorKind;
use crate::occurrence::Occurrence;

// TODO: Iterator

#[derive(PartialEq)]
pub struct Rules {
    pub rule_list: Vec<Rule>,
}

#[derive(PartialEq)]
pub struct Rule {
    pub name: String,
    pub concatenations: Concatenations,
}

impl Rule {
    pub fn new(name: String, concatenations: Concatenations) -> Rule {
        Self { name, concatenations }
    }
}


impl Debug for Rule {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        todo!()
    }
}

pub struct RulesIterator<'a> {
    rules: &'a Vec<Rule>,
}

impl Iterator for RulesIterator<'_> {
    type Item = Rule;

    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}

// impl<'a> IntoIterator for Rules {
//     type Item = Rule<'a>;
//     type IntoIter = RulesIterator<'a>;
//
//     fn into_iter(self) -> Self::IntoIter {
//         todo!()
//     }
// }

impl Rules {
    pub fn list_name(&mut self) -> &String {
        &self.rule_list.iter().next().unwrap().name
    }

    pub fn rule_iterator(&self) -> Iter<'_, Rule> {
        self.rule_list.iter()
    }
}


// Alternation will become Enum
pub type Concatenations = Vec<Concatenation>;
pub type Concatenation = Vec<ElementEnum>;


#[derive(Clone, PartialEq, Debug)]
pub enum RuleUsageKind {
    RuleToUse(String),
    Chars,
    QChars,
    Digit,
    HexDigit,
    BinDigit,
}

impl RuleUsageKind {
    pub fn create(v: String) -> RuleUsageKind {
        match v.as_str() {
            "VCHAR" => RuleUsageKind::Chars,
            "DQUOTE" => RuleUsageKind::QChars,
            "DIGIT" => RuleUsageKind::Digit,
            &_ => RuleUsageKind::RuleToUse(v),
        }
    }
}


#[derive(Clone, PartialEq, Debug)]
pub enum ElementEnum {
    RuleUsage((Occurrence, RuleUsageKind)),
    EnumDefinition,
    Group((Occurrence, Concatenations)),
    Optional((Occurrence, Concatenations)),
    LiteralValues((Occurrence, String)),
    NumberValue((Occurrence, u64)),
}


#[derive(Debug, PartialEq)]
pub enum CustomError<I> {
    WithMessage(String),
    Nom(I, ErrorKind),
}

impl CustomError<String> {
    fn append(&mut self, extension: &str) {
        match self {
            CustomError::WithMessage(msg) => {
                msg.extend(extension.chars().into_iter())
            }
            _ => {}
        }
    }
}

impl<I> nom::error::ParseError<I> for CustomError<I> {
    fn from_error_kind(input: I, kind: ErrorKind) -> Self {
        CustomError::Nom(input, kind)
    }

    fn append(_: I, _: ErrorKind, other: Self) -> Self {
        other
    }
}

impl nom::error::ParseError<&str> for CustomError<String> {
    fn from_error_kind(input: &str, _kind: ErrorKind) -> Self {
        CustomError::WithMessage(input.to_string())
    }

    fn append(input: &str, _kind: ErrorKind, mut other: Self) -> Self {
        other.append(input);
        other
    }
}

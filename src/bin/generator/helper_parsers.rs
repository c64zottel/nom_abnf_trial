use nom::branch::alt;
use nom::bytes::complete::{tag, take_while, take_while1};
use nom::character::{is_digit, is_hex_digit};
use nom::combinator::{opt, recognize};
use nom::error::{Error, ErrorKind};
use nom::IResult;
use nom::multi::{many0, many1};
use nom::sequence::tuple;

use crate::core_rules::is_bin;
use crate::grammar_rules::{c_nl, c_wsp};
use crate::rules::CustomError;

pub fn rule_list_termination_sequence() -> impl FnMut(&str) -> IResult<&str, &str, CustomError<String>> {
    move |input| {
        recognize(tuple((
            many0(c_wsp()),
            c_nl()
        )))(input)
    }
}

pub fn join_strs<'a>(mother: &'a str, first: &str, second: &str) -> Option<&'a str>
{
    let s1_start = first.as_ptr() as usize - mother.as_ptr() as usize;
    let s2_start = second.as_ptr() as usize - mother.as_ptr() as usize;
    let s1_end = s1_start + first.len();
    let s2_end = s2_start + second.len();

    if s1_end == s2_start {
        Some(&mother[s1_start..s2_end])
    } else {
        None
    }
}


pub fn join_str_vector<'a, P>(mut parser: P) -> impl FnMut(&'a str) -> IResult<&str, &str>
    where
        P: nom::Parser<&'a str, Vec<&'a str>, Error<&'a str>>,
{
    move |input| {
        match parser.parse(input) {
            Ok((remaining, result)) => {
                let mut iter = result.iter();
                let mut acc = *iter.next().unwrap();

                for s in iter {
                    if let Some(v) = join_strs(input, acc, s) {
                        acc = v;
                    } else {
                        return Err(nom::Err::Error(Error::new(input, ErrorKind::Satisfy)));
                    }
                }

                Ok((remaining, acc))
            }
            Err(e) => Err(e)
        }
    }
}


pub struct NumberTypeSelector<'a>(&'a str, fn(u8) -> bool);

impl NumberTypeSelector<'_> {
    pub(crate) const HEX: Self = Self("x", is_hex_digit);
    pub(crate) const DEC: Self = Self("d", is_digit);
    pub(crate) const BIN: Self = Self("b", is_bin);
}


// hex-val, dec-val, bin-val - depending on is_digit
pub fn some_val<'a>(number_selector: NumberTypeSelector) -> impl Fn(&str) -> IResult<&str, &str> + '_
{
    move |input| {
        match tuple((
            tag(number_selector.0),
            take_while1(|x| { number_selector.1(x as u8) }),
            opt(alt((
                join_str_vector(many1(prefixed_digits(".", number_selector.1))),
                prefixed_digits("-", number_selector.1)
            ))
            )))(input)
        {
            Ok((remaining, result)) => {
                let mut s = join_strs(input, result.0, result.1).unwrap();

                if let Some(v) = result.2 {
                    s = join_strs(input, s, v).unwrap();
                }
                Ok((remaining, s))
            }
            Err(e) => Err(e)
        }
    }
}

#[test]
fn some_val_only() {
    assert_eq!(some_val(NumberTypeSelector::DEC)("d1 "), Ok((" ", "d1")));
    assert_eq!(some_val(NumberTypeSelector::DEC)("d1."), Ok((".", "d1")));
    assert_eq!(some_val(NumberTypeSelector::DEC)("d.1 "), Err(nom::Err::Error(Error::new(".1 ", ErrorKind::TakeWhile1))));
    assert_eq!(some_val(NumberTypeSelector::DEC)("d "), Err(nom::Err::Error(Error::new(" ", ErrorKind::TakeWhile1))));
    assert_eq!(some_val(NumberTypeSelector::HEX)("x3a.b34.ef"), Ok(("", "x3a.b34.ef")));
    assert_eq!(some_val(NumberTypeSelector::BIN)("b11.0011.10 "), Ok((" ", "b11.0011.10")));
    assert_eq!(some_val(NumberTypeSelector::BIN)("x01.0011.10 "), Err(nom::Err::Error(Error::new("x01.0011.10 ", ErrorKind::Tag))));
}


fn prefixed_digits<'a, FN>(prefix: &'a str, is_digit: FN) -> impl Fn(&str) -> IResult<&str, &str> + '_
    where FN: Fn(u8) -> bool + 'a,
{
    move |input|
        match recognize(tuple((
            tag(prefix),
            take_while(|x| { is_digit(x as u8) }),
        )))(input) {
            Ok(r) => if r.1.len() < 2 {
                Err(nom::Err::Error(Error::new(input, ErrorKind::LengthValue)))
            } else {
                Ok(r)
            },
            Err(r) => Err(r)
        }
}

#[test]
fn minus_prefixed_digit() {
    let f = prefixed_digits("-", is_hex_digit);
    assert_eq!(f("-ABC34e"), Ok(("", "-ABC34e")));
    assert_eq!(f("-7"), Ok(("", "-7")));
    assert_eq!(f("-xBC34"), Err(nom::Err::Error(Error::new("-xBC34", ErrorKind::LengthValue))));
    assert_eq!(f("-LC34"), Err(nom::Err::Error(Error::new("-LC34", ErrorKind::LengthValue))));
}

#[test]
fn dot_prefixed_digit() {
    let f = prefixed_digits(".", is_hex_digit);
    assert_eq!(f(".ABC34"), Ok(("", ".ABC34")));
    assert_eq!(f(".7"), Ok(("", ".7")));
    assert_eq!(f(".xBC34"), Err(nom::Err::Error(Error::new(".xBC34", ErrorKind::LengthValue))));
    assert_eq!(f(".LC34"), Err(nom::Err::Error(Error::new(".LC34", ErrorKind::LengthValue))));
}

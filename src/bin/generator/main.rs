use std::fs::File;
use std::io::Write;
use std::str::FromStr;

use heck::{ToSnakeCase, ToUpperCamelCase};
use proc_macro2::TokenStream;
use quote::{quote, TokenStreamExt};

use crate::grammar_rules::rule_list;
use crate::parser::{ParsingData, read_literal_string};
use crate::rules::{Concatenation, ElementEnum, Rules, RuleUsageKind};

mod core_rules;
mod grammar_rules;
mod helper_parsers;
mod rules;
mod rules_dsp;
mod parser;
mod occurrence;

fn create_concatenation(rule_name: &String, ts_object_reader: &mut TokenStream, ts_fields: &mut Vec<TokenStream>, concatenation: &Concatenation) {
    // let mut ts_rule = HashMap::<&str, TokenStream>::default();

    let rule_instance = TokenStream::from_str(rule_name.to_snake_case().as_str()).unwrap();
    // let mut ts_fields = Vec::<TokenStream>::new();
    for element in concatenation {
        match element {
            ElementEnum::RuleUsage((occurrence, kind)) => {
                match kind {
                    RuleUsageKind::RuleToUse(identifier) => {
                        //cardinal?
                        // let _ts = ts_rule.entry(&rule_name).or_insert_with(|| TokenStream::default());
                        let field_name = TokenStream::from_str(identifier.to_snake_case().as_str()).unwrap();
                        let identifier = TokenStream::from_str(identifier.to_upper_camel_case().as_str()).unwrap();

                        ts_fields.push(
                            quote! {
                                    #field_name: #identifier
                                });

                        ts_object_reader.append_all(quote! {
                                    #rule_instance.#field_name = #identifier::construct( &pb );
                                });
                    }
                    RuleUsageKind::Chars => {
                        // create object
                        // create impl: read, write, display, Eq?
                        // let _ts = ts_rule.entry(&rule_name).or_insert_with(|| TokenStream::default());
                        let field_name = TokenStream::from_str(rule_name.to_snake_case().as_str()).unwrap();

                        ts_fields.push(quote! { #field_name: String });

                        // ts_object_reader.append_all(quote! {
                        //     #rule_instance.#field_name = read_arbitrary_letters(String::from("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_"), #occurrence)(&pb);
                        // });

                        ts_object_reader.append_all(quote! {
                                    read_arbitrary_letters(String::from("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_"), #occurrence)(&pb).unwrap()
                                });
                    }
                    // RuleUsageKind::QChars => {}
                    RuleUsageKind::Digit => {
                        if occurrence.contains(2u8) { // using u8
                            ts_object_reader.append_all(quote! { read_integer<u8>(2u8)(&pb).unwrap })
                        } else if occurrence.contains(3u8) || occurrence.contains(4u8) {
                            ts_object_reader.append_all(quote! { read_integer<u16>(2u8)(&pb).unwrap })
                        } else if occurrence.contains(5u8) || occurrence.contains(10u8) {
                            ts_object_reader.append_all(quote! { read_integer<u32>(2u8)(&pb).unwrap })
                        } else {
                            ts_object_reader.append_all(quote! { read_integer<u32>(2u8)(&pb).unwrap })
                        }
                        dbg!(&occurrence, &kind);
                    }
                    // RuleUsageKind::HexDigit => {}
                    // RuleUsageKind::BinDigit => {}
                    _ => { todo!(); }
                }
            }
            ElementEnum::Group((occurrence, concatenations)) => {
                let mut ts_object_reader = TokenStream::default();
                let mut ts_fields = Vec::<TokenStream>::new();
                for concatenation in concatenations {
                    create_concatenation(rule_name, &mut ts_object_reader, &mut ts_fields, concatenation);
                }
            }
            ElementEnum::Optional(_) => {}
            ElementEnum::LiteralValues((_occurrence, value)) => {
                // These values are read and ignored.
                ts_object_reader.append_all(quote! {
                                read_literal_string(#value)(&pb).unwrap();
                        });
            }
            ElementEnum::NumberValue(n) => {
                dbg!(&n);
            }
            ElementEnum::EnumDefinition => {
                println!("enum definition: {} ({})", element.to_string(), rule_name)
            }
        }
    }
}

fn create_code(rules: Rules) {
    let mut ts_objects = TokenStream::default();
    let mut ts_object_readers = TokenStream::default();
    // let mut ts_object_readers = Vec::<TokenStream>::new();
    for rule in rules.rule_iterator() {
        let mut ts_object_reader = TokenStream::default();
        let mut ts_fields = Vec::<TokenStream>::new();
        let rule_instance = TokenStream::from_str(rule.name.to_snake_case().as_str()).unwrap();

        for concatenations in &rule.concatenations {
            create_concatenation(&rule.name, &mut ts_object_reader, &mut ts_fields, concatenations);
        }

        let rule_identifier = TokenStream::from_str(rule.name.to_upper_camel_case().as_str()).unwrap();
// dbg!(rule.elements.elements.get(0).unwrap().len());
// dbg!(&rule.elements.elements);
        let mut final_ts = TokenStream::default();

        if ts_fields.len() == 1 { // generates a tuple struct
            let field_type = ts_fields.get(0).unwrap();
            let field_type = field_type.clone().into_iter().skip(2).next();

            ts_objects.append_all(
                quote! {pub struct #rule_identifier( #field_type );}
            );

            final_ts.append_all(quote! {
                impl #rule_identifier {
                pub fn construct(pb: &ParsingData) -> #rule_identifier {
                    Self(#ts_object_reader)
                }}
            });
        } else { // generate struct
            ts_objects.append_all(
                quote! {#[derive(Default)]
                    pub struct #rule_identifier { #(#ts_fields),* }}
            );
            final_ts.append_all(quote! {
                impl #rule_identifier {
                pub fn construct(pb: &ParsingData) -> #rule_identifier {
                    let mut #rule_instance = #rule_identifier::default();
                    #ts_object_reader
                    #rule_instance
                }}
            });
        }


        let ts_file = syn::parse_file(&final_ts.to_string()).unwrap();
        ts_object_readers.append_all(final_ts);
        let text = prettyplease::unparse(&ts_file);
        println!("{}", text);
    }

    let mut final_token = TokenStream::from_str("use crate::occurrence::*;
                                use crate::parser::*;").unwrap();
    ts_objects.append_all(ts_object_readers);
    final_token.append_all(ts_objects);
    let ts_file = syn::parse_file(&final_token.to_string()).unwrap();
    let text = prettyplease::unparse(&ts_file);
    File::create("src/bin/sm_parser/sm_parser_obj.rs").unwrap().write_all(text.as_bytes()).unwrap();
    let file = include_str!("../../../src/bin/generator/parser.rs");
    File::create("src/bin/sm_parser/parser.rs").unwrap().write_all(file.as_bytes()).unwrap();
    let file = include_str!("../../../src/bin/generator/occurrence.rs");
    File::create("src/bin/sm_parser/occurrence.rs").unwrap().write_all(file.as_bytes()).unwrap();
}


// Based on RFC 5234: https://www.rfc-editor.org/in-notes/std/std68.txt
fn main() {
    let schema = r#"StateMachine = "StateMachine" VERSION ID "{" [description] 1*States "}"
VERSION       = ( 2DIGIT "." 2DIGIT )
ID            = 2*VCHAR
description   = *VCHAR
States        = "State:" ID "events:" *Event
Event         = ENUM
"#;
    let pd = ParsingData::new(schema.to_string());
    let r = read_literal_string("StateMachine")(&pd);
    // let r = read_literal_string(" = ")(&pd);
    dbg!(&r);

    let (remaining, mut rules) = rule_list()(schema).unwrap();

    println!("{}", &rules.list_name());
    dbg!(remaining);
    dbg!(&rules);
    create_code(rules);
}


#[test]
fn simple_state_machine() {
    let s = include_str!("../../samples/statemachine.abnf");
    let (remaining, result) = rule_list()(s).unwrap();

    assert_eq!(remaining, "");
    dbg!(result);
}
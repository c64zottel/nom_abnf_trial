use nom::{AsChar, InputTakeAtPosition, IResult};
use nom::bytes::complete::tag;
use nom::character::complete::space1;
use nom::error::ParseError;
use crate::rules::CustomError;

// CORE RULES

// WSP 	SP / HTAB 	Space and horizontal tab
// like nom::space1
pub fn wsp<T>() -> impl FnMut(T) -> IResult<T, T, CustomError<String>>
    where
        T: InputTakeAtPosition,
        <T as InputTakeAtPosition>::Item: AsChar + Clone, CustomError<String>: ParseError<T>
{
    space1
}

pub fn is_wsp<T>(c: T) -> bool
    where T: AsChar + Clone
{
    let c = c.as_char();
    c == ' ' || c == '\t'
}

// VCHAR 	%x21–7E ('!' -  '~') Visible (printing) characters
// as String: !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`{|}~
pub fn is_vchar<T>(c: T) -> bool
    where T: AsChar + Clone
{
    let c = c.as_char();
    c >= '!' && c <= '~'
}


// CRLF
pub fn newline<'a>() -> impl FnMut(&'a str) -> IResult<&str, &str>
{
    tag("\n")
}

// ALPHA 	%x41–5A / %x61–7A 	Upper- and lower-case ASCII letters (A–Z, a–z)
// nom equivalent: nom::character::complete::alpha

// DIGIT 	%x30–39 	Decimal digits (0–9)
// HEXDIG 	DIGIT / "A" / "B" / "C" / "D" / "E" / "F" 	Hexadecimal digits (0–9, A–F, a-f)

// DQUOTE 	%x22 	Double quote
pub fn is_dquote<T>(c: T) -> bool
    where T: AsChar + Clone
{
    c.as_char() == '"'
}

pub fn dquote<'a>() -> impl FnMut(&'a str) -> IResult<&str, &str>
{
    tag("\"")
}


// SP 	%x20 	Space
pub fn is_sp<T>(c: T) -> bool
    where T: AsChar + Clone
{
    c.as_char() == ' '
}

// HTAB 	%x09 	Horizontal tab
// LWSP 	*(WSP / CRLF WSP) 	Linear white space (past newline)
// CHAR 	%x01–7F 	Any ASCII character, excluding NUL
// OCTET 	%x00–FF 	8 bits of data
// CTL 	%x00–1F / %x7F 	Controls
// CR 	%x0D 	Carriage return
// LF 	%x0A 	Linefeed

// BIT 	"0" / "1" 	Binary digit
pub fn is_bin<T>(c: T) -> bool
    where T: AsChar + Clone
{
    let c = c.as_char();
    c == '0' || c == '1'
}
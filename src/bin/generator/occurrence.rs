use std::ops::Range;

use proc_macro2::TokenStream;
use quote::{quote, TokenStreamExt, ToTokens};

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Occurrence {
    Exact(u32),
    AtLeast(u32),
    AtMost(u32),
    Range(u32, u32),
}

impl ToTokens for Occurrence {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        tokens.append_all(
            match self {
                Occurrence::Exact(x) => quote! { Occurrence::Exact(#x) },
                Occurrence::AtLeast(x) => quote! { Occurrence::AtLeast(#x) },
                Occurrence::AtMost(x) => quote! { Occurrence::AtMost(#x) },
                Occurrence::Range(x, y) => quote! { Occurrence::Range(#x,#y) },
            }
        );
    }
}

impl Occurrence {
    pub fn new_unbound() -> Occurrence {
        Occurrence::Range(0, u32::MAX)
    }
    pub fn is_upper_boundary<T>(&self, v: T) -> bool
        where
            usize: From<T>
    {
        <T as Into<usize>>::into(v) ==
            match *self {
                Occurrence::Exact(x) => x as usize,
                Occurrence::AtLeast(_) => usize::MAX,
                Occurrence::AtMost(x) => x as usize,
                Occurrence::Range(_, y) => y as usize,
            }
    }

    pub fn is_lower_boundary<T>(&self, v: T) -> bool
        where
            usize: From<T>
    {
        <T as Into<usize>>::into(v) ==
            match *self {
                Occurrence::Exact(x) => x as usize,
                Occurrence::AtLeast(x) => x as usize,
                Occurrence::AtMost(_) => 0usize,
                Occurrence::Range(x, _) => x as usize,
            }
    }

    pub fn contains<T>(&self, v: T) -> bool
        where
            usize: From<T>
    {
        let value = <T as Into<usize>>::into(v);
        match *self {
            Occurrence::Exact(x) => x as usize == value,
            Occurrence::AtLeast(x) => value >= x as usize,
            Occurrence::AtMost(x) => value <= x as usize,
            Occurrence::Range(x, y) => (x as usize) <= value && value <= (y as usize),
        }
    }

    pub fn as_range(&self) -> Range<u32> {
        let (start, end) = match *self {
            Occurrence::Exact(x) => (x, x),
            Occurrence::AtLeast(x) => (x, u32::MAX),
            Occurrence::AtMost(x) => (0u32, x),
            Occurrence::Range(x, y) => (x, y),
        };
        Range { start, end }
    }
}
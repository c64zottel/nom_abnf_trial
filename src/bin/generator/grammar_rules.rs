use nom::branch::alt;
use nom::bytes::complete::{tag, take_while};
use nom::character::complete::u32 as nom_32;
use nom::combinator::{map, opt, recognize};
use nom::error::{Error, ErrorKind};
use nom::IResult;
use nom::multi::{many0, many1, separated_list1};
use nom::sequence::{delimited, preceded, separated_pair, terminated, tuple};

use crate::core_rules::{dquote, is_dquote, is_sp, is_vchar, is_wsp, newline, wsp};
use crate::helper_parsers::{NumberTypeSelector, rule_list_termination_sequence, some_val};
use crate::occurrence::Occurrence;
use crate::rules::{Concatenations, Concatenation, CustomError, ElementEnum, Rule, Rules, RuleUsageKind};

// rulelist       =  1*( rule / (*c-wsp c-nl) )
pub fn rule_list() -> impl FnMut(&str) -> IResult<&str, Rules, CustomError<String>> {
    move |input| {
        match many1(
            terminated(
                rule(),
                opt(rule_list_termination_sequence()),
            ))(input) {
            Ok((remaining, rule_list)) => {
                let rules = Rules { rule_list };
                Ok((remaining, rules))
            }
            Err(e) => Err(e)
        }
    }
}


// rule           =  rulename defined-as elements c-nl
//                   ; continues if next line starts with white space
fn rule() -> impl FnMut(&str) -> IResult<&str, Rule, CustomError<String>> {
    move |input| {
        terminated(
            separated_pair(
                rule_name_defining(),
                defined_as(),
                elements(),
            ),
            c_nl(),
        )(input).map(|(result, (rule_name, elements))| {
            (result, Rule::new(rule_name, elements))
        })
    }
}

// rulename (as definition)     =  ALPHA *(ALPHA / DIGIT / "-")
fn rule_name_defining<'a>() -> impl FnMut(&'a str) -> IResult<&str, String, CustomError<String>>
{
    move |input| {
        if let Some(v) = input.chars().next() {
            if v.is_alphabetic() {
                let (remaining, value) = take_while::<_, &str, Error<&str>>(|x| {
                    x.is_alphanumeric() || x == '-'
                })(input).unwrap();
                return Ok((remaining, value.to_string()));
            }
        }
        Err(nom::Err::Error(CustomError::Nom(String::from("not a rule name. "), ErrorKind::Tag)))
    }
}

// rulename       =  ALPHA *(ALPHA / DIGIT / "-")
fn rule_name<'a>(occurrence: Occurrence) -> impl FnMut(&'a str) -> IResult<&str, ElementEnum, CustomError<String>>
{
    move |input| {
        if let Some(v) = input.chars().next() {
            if v.is_alphabetic() {
                let (remaining, value) = take_while::<_, &str, Error<&str>>(|x| {
                    x.is_alphanumeric() || x as char == '-'
                })(input).unwrap();
                return Ok((remaining, ElementEnum::RuleUsage((occurrence.clone(), RuleUsageKind::create(value.to_string())))));
            }
        }
        Err(nom::Err::Error(CustomError::Nom(String::from("not a rule name. "), ErrorKind::Tag)))
    }
}


// defined-as     =  *c-wsp ("=" / "=/") *c-wsp
//                   ; basic rules definition and incremental alternatives
fn defined_as() -> impl FnMut(&str) -> IResult<&str, &str, CustomError<String>> {
    move |input| {
        delimited(
            many0(c_wsp()),
            alt((tag("=/"), tag("="))), // order!
            many0(c_wsp()),
        )(input)
    }
}


// c-wsp          =  WSP / (c-nl WSP)
pub(crate) fn c_wsp<'a>() -> impl FnMut(&'a str) -> IResult<&str, &str, CustomError<String>>
{
    alt((
        recognize(wsp()),
        recognize(tuple((c_nl(), wsp()))),
    ))
}


// c-nl           =  comment / NL
pub(crate) fn c_nl<'a>() -> impl FnMut(&'a str) -> IResult<&str, &str, CustomError<String>>
{
    move |input| {
        match alt((
            comment(),
            newline(),
        ))(input) {
            Ok(x) => Ok(x),
            // I don't know how to fix this.
            Err(e) => Err(nom::Err::Error(CustomError::WithMessage(e.to_string()))),
        }
    }
}


// comment        =  ";" *(WSP / VCHAR) NL
fn comment<'a>() -> impl FnMut(&'a str) -> IResult<&str, &str>
{
    preceded(
        tag(";"),
        recognize(
            tuple((
                take_while(|x| { is_vchar(x) || is_wsp(x) }),
                newline()
            ))),
    )
}

// A vector inside a vector containing the actual elements
// The first vector are vectors separated by "/", so each vector, inside the first vector is a choice (aka alternation) of several concatenations.
// Repetition is reflected by Occurrences inside the ElementEnum.
// The second layer of vectors (elements of the first vector) consists of one or more repetitions. A repetition wraps an element joined with Occurrences (range of repetition).
// elements       =  alternation *c-wsp
fn elements() -> impl FnMut(&str) -> IResult<&str, Concatenations, CustomError<String>> {
    move |input| {
        match terminated(
            alternation(),
            many0(c_wsp()),
        )(input) {
            Ok((remaining, alternations)) => {
                if remaining.len() == 0 {
                    return Err(nom::Err::Failure(CustomError::WithMessage(String::from("Improper file ending, (forgot a new line?)"))));
                }
                Ok((remaining, alternations))
            }
            Err(e) => Err(e)
        }
    }
}


// alternation    =  concatenation
//                   *(*c-wsp "/" *c-wsp concatenation)
fn alternation() -> impl FnMut(&str) -> IResult<&str, Concatenations, CustomError<String>>
{
    move |input| {
        separated_list1(
            tuple((many0(c_wsp()), tag("/"), many0(c_wsp()))),
            concatenation())(input)
    }
}

// concatenation  =  repetition *(1*c-wsp repetition)
fn concatenation() -> impl FnMut(&str) -> IResult<&str, Concatenation, CustomError<String>>
{
    move |input| {
        match separated_list1(c_wsp(), repetition(),
        )(input) {
            Ok((remaining, elements)) => {
                Ok((remaining, elements))
            }
            Err(e) => Err(e)
        }
    }
}

// repetition     =  [repeat] element
fn repetition() -> impl FnMut(&str) -> IResult<&str, ElementEnum, CustomError<String>>
{
    move |input| {
        let (remaining, repetition) = repeat()(input).unwrap();
        match element(repetition)(remaining) {
            Ok((remaining, result)) => Ok((remaining, result)),
            Err(e) => Err(e)
        }
    }
}


// repeat         =  1*DIGIT / (*DIGIT "*" *DIGIT)
fn repeat() -> impl FnMut(&str) -> IResult<&str, Occurrence, CustomError<String>>
{
    move |input| {
        match separated_pair(
            opt(nom_32),
            tag::<&str, &str, Error<&str>>("*"),
            opt(nom_32),
        )(input)
        {
            Ok((remaining, (None, None))) => Ok((remaining, Occurrence::Range(0, u32::MAX))),
            Ok((remaining, (None, Some(max)))) => Ok((remaining, Occurrence::AtMost(max))),
            Ok((remaining, (Some(min), None))) => Ok((remaining, Occurrence::AtLeast(min))),
            Ok((remaining, (Some(min), Some(max)))) => {
                if min > max {
                    return Err(nom::Err::Failure(CustomError::WithMessage(String::from("min must be smaller or equal than max. "))));
                }
                if min == max {
                    Ok((remaining, Occurrence::Exact(min)))
                } else {
                    Ok((remaining, Occurrence::Range(min, max)))
                }
            }
            _ => {
                match nom_32::<&str, Error<&str>>(input) {
                    Ok((remaining, v)) => Ok((remaining, Occurrence::Exact(v))),
                    Err(_) => Ok((input, Occurrence::Exact(1))) // if we can not parse here - we do not have a repetition
                }
            }
        }
    }
}


// element        =  rulename / group / option /
//                   char-val / num-val / prose-val
fn element(occurrence: Occurrence) -> impl FnMut(&str) -> IResult<&str, ElementEnum, CustomError<String>>
{
    move |input| {
        alt((
            enum_val(),
            rule_name(occurrence.clone()),
            num_val(occurrence.clone()),
            char_val(occurrence.clone()),
            option(occurrence.clone()),
            group(occurrence.clone()),
        ))(input)
    }
}

fn read_alpha_string() -> impl FnMut(&str) -> IResult<&str, String, CustomError<String>> {
    move |input| {
        match input.chars().next() {
            Some(v) => {
                if v.is_alphabetic() {
                    let (remaining, value) = take_while::<_, &str, Error<&str>>(
                        |x| {
                            x.is_alphanumeric() || x == '_'
                        })(input).unwrap();
                    return Ok((remaining, value.to_string()));
                }
            }
            None => {}
        }
        Err(nom::Err::Error(CustomError::Nom(String::from("not a rule name. "), ErrorKind::Tag)))
    }
}

// enum     1*( (ALPHA *(ALPHA / DIGIT / "_") WSP))
fn enum_val() -> impl FnMut(&str) -> IResult<&str, ElementEnum, CustomError<String>> {
    // move |input| {
    //     return match terminated(tag("ENUM"), newline())(input) {
    //         Ok((remaining, _)) => Ok((remaining, ElementEnum::EnumDefinition)),
    //         Err(e) => Err(nom::Err::Error(CustomError::WithMessage(e.to_string()))),
    //     };
    // }
    move |input| {
        return match tag::<&str, &str, CustomError<String>>("ENUM")(input) {
            Ok((remaining, _)) => Ok((remaining, ElementEnum::EnumDefinition)),
            Err(e) => Err(nom::Err::Error(CustomError::WithMessage(e.to_string()))),
        };
    }
}


// option         =  "[" *c-wsp alternation *c-wsp "]"
fn option(occurrence: Occurrence) -> impl FnMut(&str) -> IResult<&str, ElementEnum, CustomError<String>>
{
    move |input| {
        match delimited(
            tuple((tag("["), many0(c_wsp()))),
            alternation(),
            tuple((many0(c_wsp()), tag("]"))),
        )(input) {
            Ok((remaining, value)) => Ok((remaining, ElementEnum::Optional((occurrence.clone(), value)))),
            Err(e) => Err(e),
        }
    }
}


// group          =  "(" *c-wsp alternation *c-wsp ")"
fn group(occurrence: Occurrence) -> impl FnMut(&str) -> IResult<&str, ElementEnum, CustomError<String>>
{
    move |input| {
        match delimited(
            tuple((tag("("), many0(c_wsp()))),
            alternation(),
            tuple((many0(c_wsp()), tag(")"))),
        )(input) {
            Ok((remaining, value)) => Ok((remaining, ElementEnum::Group((occurrence.clone(), value)))),
            Err(e) => Err(e),
        }
    }
}

// char-val       =  DQUOTE *(%x20-21 / %x23-7E) DQUOTE
//                   ; quoted string of SP and VCHAR without DQUOTE
fn char_val(occurrence: Occurrence) -> impl Fn(&str) -> IResult<&str, ElementEnum, CustomError<String>> {
    move |input| {
        match delimited(
            dquote(),
            take_while(|c| {
                is_sp(c) || (is_vchar(c) && !is_dquote(c))
            }),
            dquote(),
        )(input) {
            Ok((remaining, value)) => Ok((remaining, ElementEnum::LiteralValues((occurrence.clone(), value.to_string())))),
            Err(e) => Err(nom::Err::Error(CustomError::WithMessage(e.to_string()))),
        }
    }
}


// num-val        =  "%" (bin-val / dec-val / hex-val)
//
// bin-val        =  "b" 1*BIT
//                   [ 1*("." 1*BIT) / ("-" 1*BIT) ]
//
// dec-val        =  "d" 1*DIGIT
//                   [ 1*("." 1*DIGIT) / ("-" 1*DIGIT) ]
//
// hex-val        =  "x" 1*HEXDIG
//                   [ 1*("." 1*HEXDIG) / ("-" 1*HEXDIG) ]
fn num_val(occurrence: Occurrence) -> impl Fn(&str) -> IResult<&str, ElementEnum, CustomError<String>> {
    move |input| {
        match preceded(
            tag("%"),
            alt((
                map(some_val(NumberTypeSelector::DEC), |x: &str| x[1..].parse().unwrap()),
                map(some_val(NumberTypeSelector::HEX), |x: &str| {
                    u64::from_str_radix(&x[1..], 16).unwrap()
                }),
                map(some_val(NumberTypeSelector::BIN), |x: &str| u64::from_str_radix(&x[1..], 2).unwrap()),
            )),
        )(input) {
            Ok((remaining, value)) => Ok((remaining, ElementEnum::NumberValue((occurrence.clone(), value)))),
            Err(e) => Err(nom::Err::Error(CustomError::WithMessage(e.to_string())))
        }
    }
}


// prose-val      =  "<" *(%x20-3D / %x3F-7E) ">"
//                     ; bracketed string of SP and VCHAR  without angles
//                     ; prose description, to be used as last resort
fn prose_val() -> impl FnMut(&str) -> IResult<&str, &str> {
    move |input| {
        recognize(
            delimited(
                tag("<"),
                take_while(|c| { (c >= '\u{20}' && c <= '\u{3d}') || (c >= '\u{3f}' && c <= '\u{7e}') }),
                tag(">"),
            ))(input)
    }
}

#[test]
fn rule_list_() {
    use crate::grammar_rules::ElementEnum::{Group, LiteralValues, Optional, RuleUsage};
    let abnf = include_str!("../../samples/postal-address.abnf"); // example taken from wikipedia
    assert_eq!(rule_list()(abnf), Ok(("", Rules {
        rule_list:
        vec![
            (RuleName("postal-address".to_string()),
             Elements {
                 elements: vec![vec![RuleUsage((Occurrence::Exact(1), "name-part".to_string())), RuleUsage((Occurrence::Exact(1), "street".to_string())), RuleUsage((Occurrence::Exact(1), "zip-part".to_string()))]]
             }),
            (RuleName("name-part".to_string()),
             Elements {
                 elements: vec![vec![Group((Occurrence::Range(0, u32::MAX), vec![vec![RuleUsage((Occurrence::Exact(1), "personal-part".to_string())), RuleUsage((Occurrence::Exact(1), "SP".to_string()))]])),
                                     RuleUsage((Occurrence::Exact(1), "last-name".to_string())),
                                     Optional((Occurrence::Exact(1), vec![vec![RuleUsage((Occurrence::Exact(1), "SP".to_string())), RuleUsage((Occurrence::Exact(1), "suffix".to_string()))]])),
                                     RuleUsage((Occurrence::Exact(1), "CRLF".to_string()))]]
             }),
            (RuleName("name-part".to_string()),
             Elements {
                 elements:
                 vec![vec![RuleUsage((Occurrence::Exact(1), "personal-part".to_string())), RuleUsage((Occurrence::Exact(1), "CRLF".to_string()))]]
             }),
            (RuleName("personal-part".to_string()),
             Elements {
                 elements:
                 vec![vec![RuleUsage((Occurrence::Exact(1), "first-name".to_string()))],
                      vec![Group((Occurrence::Exact(1), vec![vec![RuleUsage((Occurrence::Exact(1), "initial".to_string())), LiteralValues((Occurrence::Exact(1), ".".to_string()))]]))],
                 ]
             }),
            (RuleName("first-name".to_string()),
             Elements {
                 elements:
                 vec![vec![RuleUsage((Occurrence::Range(0, u32::MAX), "ALPHA".to_string()))]]
             }),
            (RuleName("initial".to_string()), Elements { elements: vec![vec![RuleUsage((Occurrence::Exact(1), "ALPHA".to_string()))]] }),
            (RuleName("last-name".to_string()), Elements { elements: vec![vec![RuleUsage((Occurrence::Range(0, u32::MAX), "ALPHA".to_string()))]] }),
            (RuleName("suffix".to_string()),
             Elements {
                 elements:
                 vec![vec![
                     Group((Occurrence::Exact(1), vec![
                         vec![LiteralValues((Occurrence::Exact(1), "Jr.".to_string()))],
                         vec![LiteralValues((Occurrence::Exact(1), "Sr.".to_string()))],
                         vec![Group((Occurrence::AtLeast(1), vec![
                             vec![LiteralValues((Occurrence::Exact(1), "I".to_string()))],
                             vec![LiteralValues((Occurrence::Exact(1), "V".to_string()))],
                             vec![LiteralValues((Occurrence::Exact(1), "X".to_string()))],
                         ]))], ]))]]
             }),
            (RuleName("street".to_string()),
             Elements {
                 elements:
                 vec![vec![
                     Optional((Occurrence::Exact(1), vec![vec![RuleUsage((Occurrence::Exact(1), "apt".to_string())), RuleUsage((Occurrence::Exact(1), "SP".to_string()))]])),
                     RuleUsage((Occurrence::Exact(1), "house-num".to_string())),
                     RuleUsage((Occurrence::Exact(1), "SP".to_string())),
                     RuleUsage((Occurrence::Exact(1), "street-name".to_string())),
                     RuleUsage((Occurrence::Exact(1), "CRLF".to_string()))]]
             }),
        ]
    })));
//         (RuleDefinition( "apt".to_string())), vec![vec![RuleUsage((Occurrence::Range(1, 4), "DIGIT".to_string()))]])],
//         (RuleDefinition( "house-num".to_string())), vec![vec![Group((Occurrence::Range(1, 8), vec![vec![RuleUsage((Occurrence::Exact(1), "DIGIT".to_string()))], vec![RuleUsage((Occurrence::Exact(1), "ALPHA".to_string()))]]))]])],
//         (RuleDefinition( "street-name".to_string())), vec![vec![RuleUsage((Occurrence::AtLeast(1), "VCHAR".to_string()))]])],
//         (RuleDefinition( "zip-part".to_string())), vec![vec![
//             RuleUsage((Occurrence::Exact(1), "town-name".to_string())),
//             CharValue((Occurrence::Exact(1), ",".to_string())),
//             RuleUsage((Occurrence::Exact(1), "SP".to_string())),
//             RuleUsage((Occurrence::Exact(1), "state".to_string())),
//             RuleUsage((Occurrence::Range(1, 2), "SP".to_string())),
//             RuleUsage((Occurrence::Exact(1), "zip-code".to_string())),
//             RuleUsage((Occurrence::Exact(1), "CRLF".to_string()))]])],
//         (RuleDefinition( "town-name".to_string())), vec![vec![Group((Occurrence::AtLeast(1), vec![vec![RuleUsage((Occurrence::Exact(1), "ALPHA".to_string()))], vec![RuleUsage((Occurrence::Exact(1), "SP".to_string()))]]))]])],
//        (RuleDefinition( "state".to_string())), vec![vec![RuleUsage((Occurrence::Exact(2), "ALPHA".to_string()))]])],
//         (RuleDefinition( "zip-code".to_string())), vec![vec![
//             RuleUsage((Occurrence::Exact(5), "DIGIT".to_string())),
//             Optional((Occurrence::Exact(1), vec![vec![CharValue((Occurrence::Exact(1), "-".to_string())), RuleUsage((Occurrence::Exact(4), "DIGIT".to_string()))]]))]]
//         )]
// })));


// abnf cannot parse it's own rules, because there is no concept of range (see char-val (%x20-21))
// let abnf = include_str!("samples/abnf.abnf");
// assert_eq!(rule_list()(abnf), Ok(("", vec![vec![]])));

// ABNF expects a clean NL at the very end - no additional space is allowed.
    let abnf = "postal-address   = name-part\n ";
    assert_eq!(rule_list()(abnf),
               Err(nom::Err::Failure(CustomError::WithMessage(String::from("Improper file ending, (forgot a new line?)")))));
}

#[test]
fn rule_() {
    let r = "rule           =  rulename defined-as elements c-nl
                                ; continues if next line starts
                                ;  with white space\n";
    assert_eq!(rule()(r), Ok(("", (
        RuleName("rule".to_string()),
        Elements {
            elements: vec![vec![
                ElementEnum::RuleUsage((Occurrence::Exact(1), "rulename".to_string())),
                ElementEnum::RuleUsage((Occurrence::Exact(1), "defined-as".to_string())),
                ElementEnum::RuleUsage((Occurrence::Exact(1), "elements".to_string())),
                ElementEnum::RuleUsage((Occurrence::Exact(1), "c-nl".to_string())),
            ]]
        })
    )));
}

#[test]
fn rule_name_() {
    assert_eq!(rule_name(Occurrence::Exact(1))("first-rule").unwrap(), ("", ElementEnum::RuleUsage((Occurrence::Exact(1), "first-rule".to_string()))));

    assert_eq!(rule_name(Occurrence::Exact(1))("1this-is-not-good"),
               Err(nom::Err::Error(
                   CustomError::Nom("not a rule name. ".to_string(), ErrorKind::Tag)
               ))
    );
}

#[test]
fn define_as_() {
    let s = " = ";
    assert_eq!(defined_as()(s).unwrap(), ("", "="));

    // no WSP after NL, so no match
    assert_eq!(defined_as()(" =/ \n").unwrap(), ("\n", "=/"));

    // WSP after NL, so a match!
    assert_eq!(defined_as()(" =/ \n ").unwrap(), ("", "=/"));
}

#[test]
fn c_wsp_() {
    let ws_space = " ";
    let (remaining, _result) = c_wsp()(ws_space).unwrap();
    assert_eq!(remaining, "");

    let ws_htab = "\t";
    let (remaining, _result) = c_wsp()(ws_htab).unwrap();
    assert_eq!(remaining, "");

    let c_nl_wsp = "; comment with WSP\n ";
    let (remaining, _result) = c_wsp()(c_nl_wsp).unwrap();
    assert_eq!(remaining, "");

    let c_nl_wsp = " \n   \n \t";
    assert_eq!(many0(c_wsp())(c_nl_wsp).unwrap(),
               ("",
                Vec::from([" ", "\n   ", "\n \t", ])
               ));
}

#[test]
fn comment_() {
    let c = "; some correct comment with newline\nnot_included";
    let (remaining, result) = comment()(c).unwrap();

    assert_eq!(result, " some correct comment with newline\n");
    assert_eq!(remaining, "not_included");
}

#[test]
fn elements_() {

    // double quotes are not preserved and WSP + NL is swallowed
    // by elements - this is the correct behaviour
    // (well, one may argue about the DQUOTE)
    assert_eq!(elements()("\"hm\"\n"), Ok(("\n", Elements {
        elements: vec![
            vec![ElementEnum::LiteralValues((Occurrence::Exact(1), "hm".to_string()))]
        ]
    })));

    assert_eq!(elements()("Rule1 Rule2   \n"), Ok(("\n", Elements {
        elements: vec![
            vec![
                ElementEnum::RuleUsage((Occurrence::Exact(1), "Rule1".to_string())),
                ElementEnum::RuleUsage((Occurrence::Exact(1), "Rule2".to_string())),
            ]
        ]
    })));

    assert_eq!(elements()("5*Rule11*ruly-de-duly\n"), Ok(("*ruly-de-duly\n", Elements {
        elements: vec![
            vec![ElementEnum::RuleUsage((Occurrence::AtLeast(5), "Rule11".to_string()))]
        ]
    })));
}

#[test]
fn alternation_() {
    assert_eq!(alternation()("\"hm\""), Ok(("", vec![
        vec![(ElementEnum::LiteralValues((Occurrence::Exact(1), "hm".to_string())))]])));

    assert_eq!(alternation()("ruleusage / 7*group / option"), Ok(("", vec![
        vec![ElementEnum::RuleUsage((Occurrence::Exact(1), "ruleusage".to_string()))],
        vec![ElementEnum::RuleUsage((Occurrence::AtLeast(7), "group".to_string()))],
        vec![ElementEnum::RuleUsage((Occurrence::Exact(1), "option".to_string()))],
    ])));

    assert_eq!(alternation()("Rule1 Rule2"), Ok(("", vec![
        vec![
            ElementEnum::RuleUsage((Occurrence::Exact(1), "Rule1".to_string())),
            ElementEnum::RuleUsage((Occurrence::Exact(1), "Rule2".to_string())),
        ]
    ])));

    assert_eq!(alternation()("5*Rule11*ruly-de-duly"), Ok(("*ruly-de-duly", vec![
        vec![
            ElementEnum::RuleUsage((Occurrence::AtLeast(5), "Rule11".to_string())),
        ]
    ])));
}

#[test]
fn concatenation_() {
    assert_eq!(concatenation()("\"hm\""),
               Ok(("", vec![ElementEnum::LiteralValues((Occurrence::Exact(1), "hm".to_string()))])));
    assert_eq!(concatenation()("5*\"hm\" ruly-de-duly"),
               Ok(("", vec![
                   ElementEnum::LiteralValues((Occurrence::AtLeast(5), "hm".to_string())),
                   ElementEnum::RuleUsage((Occurrence::Exact(1), "ruly-de-duly".to_string())),
               ])));
    assert_eq!(concatenation()("*5\"hm\" 1ruly-de-duly"),
               Ok(("", vec![
                   ElementEnum::LiteralValues((Occurrence::AtMost(5), "hm".to_string())),
                   ElementEnum::RuleUsage((Occurrence::Exact(1), "ruly-de-duly".to_string())),
               ])));
    // Do we allow this?
    // assert_eq!(concatenation()("5*3\"hm\""), Ok(("", ElementEnum::NameValue("5*3\"hm\"".to_string()))));
}

#[test]
fn repetition_() {
    assert_eq!(repetition()("something"), Ok(("", ElementEnum::RuleUsage((Occurrence::Exact(1), "something".to_string())))));
    assert_eq!(repetition()("5something"), Ok(("", ElementEnum::RuleUsage((Occurrence::Exact(5), "something".to_string())))));
}

#[test]
fn repeat_() {
    assert_eq!(repeat()("5Something"), Ok(("Something", (Occurrence::Exact(5)))));
    assert_eq!(repeat()("2*5Something"), Ok(("Something", (Occurrence::Range(2, 5)))));
    assert_eq!(repeat()("6*5Something"), Err(nom::Err::Failure(WithMessage("min must be smaller or equal than max. ".to_string()))));
    assert_eq!(repeat()("*5Something"), Ok(("Something", (Occurrence::AtMost(5)))));
    assert_eq!(repeat()("1*Something"), Ok(("Something", (Occurrence::AtLeast(1)))));
}

#[test]
fn element_() {
    assert_eq!(element(Occurrence::Exact(1))("(  asdf  )"),
               Ok(("", ElementEnum::Group(
                   (Occurrence::Exact(1), vec![vec![ElementEnum::RuleUsage((Occurrence::Exact(1), "asdf".to_string()))]]))))
    );
    assert_eq!(element(Occurrence::Exact(1))("(asdf)"),
               Ok(("", ElementEnum::Group(
                   (Occurrence::Exact(1), vec![vec![ElementEnum::RuleUsage((Occurrence::Exact(1), "asdf".to_string()))]])
               ))));

    assert_eq!(element(Occurrence::Exact(1))("[  asdf \n ]"),
               Ok(("", ElementEnum::Optional(
                   (Occurrence::Exact(1), vec![vec![ElementEnum::RuleUsage((Occurrence::Exact(1), "asdf".to_string()))]])
               ))));

    assert_eq!(element(Occurrence::Exact(1))("rule1 / rule2 / rule3"),
               Ok((" / rule2 / rule3", ElementEnum::RuleUsage((Occurrence::Exact(1), "rule1".to_string())))));
}

#[test]
fn char_val_() {
    assert_eq!(char_val(Occurrence::Exact(1))("\"some _ text\""), Ok(("", ElementEnum::LiteralValues((Occurrence::Exact(1), "some _ text".to_string())))));
    assert_eq!(char_val(Occurrence::Exact(1))("\"some \" text\""), Ok((" text\"", ElementEnum::LiteralValues((Occurrence::Exact(1), "some ".to_string())))));
}

#[test]
fn num_val_() {
    assert_eq!(num_val(Occurrence::Exact(1))("%d33"), Ok(("", ElementEnum::NumberValue((Occurrence::Exact(1), 33u64)))));
    assert_eq!(num_val(Occurrence::Exact(1))("%x27"), Ok(("", ElementEnum::NumberValue((Occurrence::Exact(1), 0x27u64)))));
    assert_eq!(num_val(Occurrence::Exact(1))("%b1011011"), Ok(("", ElementEnum::NumberValue((Occurrence::Exact(1), 0b1011011u64)))));
}

#[test]
fn prose_val_() {
    assert_eq!(prose_val()("< asd*K\"f>"), Ok(("", "< asd*K\"f>")));
}
use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use crate::occurrence::Occurrence;

use crate::rules::{ElementEnum, Rules};

impl Debug for Rules {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "\n").unwrap();
        for rule in &self.rule_list {
            write!(f, "Rule: {}\n\t\tElements:", rule.name).unwrap();

            for rule in &rule.concatenations {
                for x in rule {
                    write!(f, "\t\t\t{}", x).unwrap();
                }
            }
            write!(f, "\n").unwrap();
        }
        Ok(())
    }
}


impl Display for Occurrence {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Occurrence::Exact(x) => write!(f, "{x}x"),
            Occurrence::AtLeast(x) => write!(f, "{x}.."),
            Occurrence::AtMost(x) => write!(f, "..{x}"),
            Occurrence::Range(x, y) => write!(f, "{x}..{y}"),
        }
    }
}

impl Display for ElementEnum {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            ElementEnum::RuleUsage((repeat, name)) => {
                write!(f, "RuleUsage({}, {:?})", repeat, name)
            }
            ElementEnum::Group((repeat, elements)) => {
                write!(f, "Group({}, ", repeat).unwrap();
                for x in elements {
                    for i in x {
                        write!(f, "{}", i).unwrap();
                    }
                }
                write!(f, " )").unwrap();
                Ok(())
            }
            ElementEnum::Optional((repeat, elements)) => {
                write!(f, "Optional({}, ", repeat).unwrap();
                for x in elements {
                    for i in x {
                        write!(f, "{}", i).unwrap();
                    }
                }
                write!(f, " )").unwrap();
                Ok(())
            }
            ElementEnum::LiteralValues((repeat, value)) => {
                write!(f, "LiteralValues({}, {})", repeat, value)
            }
            ElementEnum::NumberValue((repeat, value)) => {
                write!(f, "NumberValue({}, {})", repeat, value)
            }
            ElementEnum::EnumDefinition => {
                write!(f, "EnumDefinition()")
            }
        }
    }
}
